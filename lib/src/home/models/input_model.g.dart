// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'input_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_InputModel _$$_InputModelFromJson(Map<String, dynamic> json) =>
    _$_InputModel(
      stdInArgs: json['stdInArgs'] as String,
      cmdLineArgs: json['cmdLineArgs'] as String,
    );

Map<String, dynamic> _$$_InputModelToJson(_$_InputModel instance) =>
    <String, dynamic>{
      'stdInArgs': instance.stdInArgs,
      'cmdLineArgs': instance.cmdLineArgs,
    };
