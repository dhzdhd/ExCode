// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'snippet_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_SnippetModel _$$_SnippetModelFromJson(Map<String, dynamic> json) =>
    _$_SnippetModel(
      name: json['name'] as String,
      value: json['value'] as String,
      length: json['length'] as int,
    );

Map<String, dynamic> _$$_SnippetModelToJson(_$_SnippetModel instance) =>
    <String, dynamic>{
      'name': instance.name,
      'value': instance.value,
      'length': instance.length,
    };
